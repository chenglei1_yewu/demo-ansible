
all:
	./build.sh
install:
	./install.sh
test:
#	./test.sh 149_28
	./test.sh slurmtest
clean:
	rm -rf demo_ansible.egg-info/ build/ dist/
distclean:
	rm -rf demo_ansible.egg-info/ build/ dist/ usr1/

