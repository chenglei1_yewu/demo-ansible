#!/bin/bash

cdir=$(pwd)

if [ $# -eq 0 ] ; then
    echo -e "Usage: $0 <dir in ${cdir}/example/>"
    read t dumb <<< $(ls ${cdir}/example/)
    echo -e "Example:"
    echo -e "\t$0 $t"
    echo -e "\n# ls ${cdir}/example/"
    ls ${cdir}/example/
    echo
    exit 0
fi

dest="$1"
shift

cd usr1 && . bin/activate || exit 1

for act in bootstrap-servers prechecks deploy; do
#for act in deploy; do
    demo-ansible $act \
      --inventory ${cdir}/example/$dest/multinode \
      --configdir ${cdir}/example/$dest \
      --passwords ${cdir}/example/$dest/passwords.yml $* || \
      exit 1
done
