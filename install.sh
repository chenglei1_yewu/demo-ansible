#!/bin/bash

set -e

once=false
ver=$(cat ./demo_ansible.egg-info/PKG-INFO |grep "^Version"|awk '{print $2}')
[ -n "$ver" ] || ver="0.0.1.dev1"

dist=demo_ansible-${ver}-py2-none-any.whl

if [ ! -d usr1 ]; then
    once=true
fi

if $once; then
    virtualenv usr1
    cd usr1/
    . bin/activate
    pip install --upgrade pip
    pip install ansible
    pip install ../dist/$dist
else
    cd usr1/
    . bin/activate
    pip install ../dist/$dist
    pip install --no-deps --upgrade --force-reinstall ../dist/$dist
fi

echo "run: sudo make test"
