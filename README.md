## slurm测试环境部署

说明：部署环境是CentOS 7.5。更高版本的CentOS 7.x应该也可以。

slurm节点一般是物理机。测试可以用虚拟机。建议至少三个slurm节点。

deploy节点负责构建deploy环境和运行ansible。ansible只需要位于deploy节点，但不必提前手动安装ansible。deploy节点可以跟slurm共用同一个节点，但非必须。

mariadb节点部署的是mariadb-server。不要提前手动安装mysql。

提前在各个节点之间建立ssh互信。

### 构建deploy环境

在deploy节点执行。

```
    # make
    # make install
```

slurm部署配置文件位于 examples/slurmtest/，该目录下的文件需要进一步编辑。

*注意*：本项目是从kolla移植来的，部分无用的代码没有移除。

### 编辑globals.yml

enable_slurm 设置为 yes

enable_mariadb 设置为 yes

enable_slurmrestd 设置为 yes

enable_host_ntp 建议设置为 yes

slurmdbd_database_address slurmdbd数据库服务器IP地址。

如果用OpenStack云主机作为slurmdbd节点，将slurmdbd_database_address设置为云主机的fixed ip地址。


### 编辑 multinode

主要的group如下

* baremetal 包含所有的物理节点
* mariadb 数据库服务器
* slurm 所有的slurm服务节点，包括slurmctld，slurmd，slurmdbd，slurmrestd等。
* slurmservers 即slurmctld节点
* slurmdbdservers 即slurmdbd节点
* slurmexechosts 即slurmd节点，用于计算。
* slurmrestservers 即slurmrestd节点。

建议 mariadb slurmservers slurmdbdservers slurmrestservers 都用一个节点。

mariadb:vars 变量的含义

* db_interface 数据库在这个接口上监听。例如eth0。根据实际情况修改。
* db_address_family 数据库服务的IP协议版本，一般是ipv4

slurm:vars 变量的含义

* network_interface 提供服务的接口名称，例如eth0。根据实际情况修改。
* enable_slurm 是否启用slurn，必须设置为yes

### 编辑 passwords.yml

* mysql_root_password 数据库的root密码
* slurmdbd_database_pass 数据库slurmdbd访问的密码
* password1没有用到，可以删除该项。

### 需要打开的端口

如果部署在OpenStack云主机上，须注意安全组的设置。需要打开的端口有：

* tcp 6817  slurmctld
* tcp 6818  slurmd
* tcp 6819  slurmdbd
* tcp 6820  slurmrestd
* tcp 3306  mysqld

其中6820端口需要开放给外部网络。其他端口开放给slurm集群内部访问。

### 部署

目前没有实现用户统一管理，例如通过ypserv。
但在所有slurm节点创建一致的用户slurmtest01，slurmtest02。

```
    # sudo make test
```

或者直接执行

```
./test.sh slurmtest
```

### 测试

```bash
[root@host-144-12 ~]# srun -N4 -n4 -l hostname
0: host-144-12
3: host-144-16
2: host-144-15
1: host-144-14
```

在slurm control节点创建一个hosts文件，例如，获取机器列表

```bash
# srun -N-99999 hostname |sort -n |tee hosts 
host-144-12
host-144-14
host-144-15
host-144-16
```

通常在所有计算节点都要部署计算程序

运行一个简单测试calc-pi.sh

```bash
[slurmtest01@i-slurm-1 mpitest]$ whoami
slurmtest01
[slurmtest01@i-slurm-1 mpitest]$ pwd
/home/slurmtest01/mpitest
[slurmtest01@i-slurm-1 mpitest]$ sbatch calc-pi.sh
Submitted batch job 24
[slurmtest01@i-slurm-1 mpitest]$ squeue 
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
                24     debug   CalcPi slurmtes  R       0:02      3 i-slurm-[1-3]

[slurmtest01@i-slurm-1 mpitest]$ squeue 
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
[slurmtest01@i-slurm-1 mpitest]$ cat logs/job.24.out 
Elapsed time = 15.329501 seconds 
Pi is approximately 3.1415788983418094, Error is 0.0000137552479837
```

### 验证slurmrest

在slurm控制节点执行

```bash
# scontrol token lifespan=31536000 username=root
```

记下获取到的token。

然后到客户端节点测试REST API

```bash

[root@gb21 ~]# token=eyJ...ONY
[root@gb21 ~]# server=10.10.144.12
[root@gb21 ~]# user=root
[root@gb21 ~]# curl -X GET \
 -H "X-SLURM-USER-NAME:$user" \
 -H "X-SLURM-USER-TOKEN:$token" \
 http://$server:6820/openapi/v3/
```

## 源码

demo_ansible/ demo_ansible的python包

tools/demo-ansible 命令行脚本工具

example/ 工作目录，在其中创建子目录，并保存特定部署的配置文件globals.yml等

test.sh 测试脚本，多个action的执行

