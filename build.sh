#!/bin/bash

set -e

once=false

if [ ! -d usr1 ]; then
    once=true
fi

if $once; then
    virtualenv usr1
    . usr1/bin/activate
    pip install --upgrade pip
    pip install pbr
    pip install ansible
    python setup.py bdist_wheel
else
    . usr1/bin/activate
    pip show pbr || { pip install pbr; }
    pip show ansible || { pip install ansible; }
    python setup.py bdist_wheel
fi


